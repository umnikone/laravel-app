## Great Laravel App
#### Installation
1. Extract all files
2. Copy and Rename **.env.example** to **.env**
3. Edit **.env**
	- Set **App_Url**
	- Set **DB_DATABASE**, **DB_USERNAME**, **DB_PASSWORD**
	- Set  all **MAIL_* config**

4. Open console and run:
	- **composer install**
	- **npm install**
	- **php artisan migrate**
	- **npm run dev**
	- **php artisan config:clear**

